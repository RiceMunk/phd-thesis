\chapter[Observations]{Observations}
\label{chap:observations}
\chaptermark{Observations}

Archival observational data from a number of telescopes form the cornerstone of this thesis.
The two telescopes which play the most prominent roles in this thesis --AKARI and Herschel-- are both space observatories which reached the end of their operational lifespan. In this chapter the operations and instrumentation of both these telescopes are detailed in their own sections.
Additional archival data was acquired and exploited from the retired ISAAC spectrometer of the Very Large Telescope (VLT; a ground-based telescope located in Chile), and from the the JxA and JxB (both also retired) instruments of the James Clerk Maxwell Telescope (JCMT; Hawaii). 
A number of other archival datasets appear sporadically in other results chapters, such as data from the Spitzer and the Wide Infrared Survey Explorer (WISE) space telescopes. The main purpose of this chapter is to give the reader a brief overview of the methods of data acquisition and additionally the rationale for the observations which provide the basis of my thesis work.
\newpage

\section{The AKARI space telescope}
\begin{figure}
 \includegraphics[width=0.95\linewidth]{./graph/observations/akari_schematic.png}
 \caption[Schematic of the AKARI space telescope]{A schematic view of the AKARI space telescope. The numeric scales are in millimetres. This figure has been reproduced from \citet{ASTRO-F_manual} Figure 2.2.1.}
 \label{fig:observations_akari_schematic}
\end{figure}

I exploit AKARI data in Chapters \ref{chap:cometh} and \ref{chap:icemap} of this thesis. Furthermore, I developed the pipeline \arf2 --documented in Appendix \ref{chap:software} at the end of the thesis-- specifically to reduce AKARI spectroscopic data.

\subsection{Mission summary}
AKARI (previously known as ASTRO-F and before that as IRIS) was a space telescope operated by the Institute of Space and Astronautical Science of the Japanese Aerospace Exploration Agency (ISAS/JAXA) in collaboration with European and Korean institutes with the primary goal of performing an all-sky survey in the mid- and far- infrared.  The main equipment onboard AKARI consisted of a 68.5 cm primary mirror and two instruments: the Far-Infrared Surveyor (FIS) and the InfraRed Camera (IRC) \citep{ASTRO-F_manual}. Both instruments had both imaging and spectroscopic capabilities, although the available observing modes were primarily developed for imaging. The primary mirror and instruments of AKARI were cooled to cryogenic temperatures using onboard reserves of liquid helium.

AKARI was launched on the 21$^{\rm st}$ Febrary 2006\footnote{\url{http://www.ir.isas.jaxa.jp/ASTRO-F/Observation/Newsletter/afnl_e_014.txt}} into a polar orbit around Earth, at a height of $\sim$745 km. The lifetime operational stages of AKARI consisted of three phases preceded by a performance verification phase. Following the eponymous performance verification (PV) phase, phase 1 consisted of performing the all-sky survey, which was the primary mission of AKARI. The PV phase lasted for 2 months and phase 1 lasted for another 6 months, with phase 2 observations starting in November 2006. Phase 2 observations consisted of additional all-sky survey observations but also of other pointed observations carried out as part of AKARI's open-time programme. Phase 2 lasted until August 2007 when AKARI ran out of liquid coolant, marking the end of phase 2 and the beginning of phase 3. Phase 3 consisted of additional open-time pointed observations, but with only the near-infrared (NIR) capabilities of the IRC being used due to both FIS and the mid-infrared (MIR) components of IRC being rendered virtually useless by the increased temperature of AKARI. Phase 3 lasted until June 2011, following a power-supply malfunction which happened in May 2011. The transmitters aboard AKARI were turned off on November 24$^{\rm th}$ 2011, marking the end of the AKARI mission\footnote{\url{http://www.isas.jaxa.jp/e/topics/2011/1124_akari.shtml}}.
\subsection{Instrument overview}
The AKARI spacecraft carried two instruments: the Far-Infrared Surveyor (FIS) and the InfraRed Camera (IRC).
\subsubsection{the Far-Infrared Surveyor -- FIS}
The Far-Infrared Surveyor \citep{AKARI-FIS_manual} was the instrument aboard AKARI which specialized in observing at a far-infrared wavelength range of 50 to 180 microns. The instrument was capable of performing photometric observations at four distinct bands, and could use a Martin-Puplett Fourier transform spectrometer to perform high-spectral-resolution spectroscopy in two separate bands.

The FIS holds no direct relevance to this thesis, and thus will not be discussed further. For more information on the instrument, see \citet{AKARI-FIS_manual}.

\subsubsection{The InfraRed Camera -- IRC}
\begin{figure}
 \includegraphics[width=0.95\linewidth]{./graph/observations/akari_irc_schematic.png}
 \caption[Schematic view of the AKARI IRC instrument]{A schematic view of the AKARI IRC instrument. This figure has been reproduced from \citet{AKARI-IRC_manual} Figure 2.0.1.}
 \label{fig:observations_akari_irc_schematic}
\end{figure}
The InfraRed Camera \citep{AKARI-IRC_manual,Ohyama2007} was the second instrument aboard AKARI, and it specialized in the observing in the near- and mid-infrared wavelength ranges, between 1.8 and 26.5 microns. As shown in the schematic view of the instrument in Figure \ref{fig:observations_akari_irc_schematic}, it consisted of three separate detectors: one for the near-infrared (NIR; $1.8\ldots 5.3\, {\rm \mu m}$) and two for the mid-infrared (MIR), split between "short" (MIR-S; $5.4\ldots 13.1\, {\rm \mu m}$) and "long" (MIR-L; $12.6\ldots 26.5\, {\rm \mu m}$) MIR. Each detector was capable of both imaging and multi-object spectroscopy observations in a field of view roughly $10\arcmin \times 10\arcmin$ in size, with the exact size of the field of view varying between detector and filter, as illustrated in Figure \ref{fig:observations_akari_irc_fovs}. For the sake of simplicity, all further references to the large ($\sim 10\arcmin\times 10\arcmin$) field of view in this thesis will be called the $10\arcmin \times 10\arcmin$ field of view, despite the precise size of the frame not being this in any detector/filter combination. In addition to the $10\arcmin \times 10\arcmin$ field of view, the AKARI detector also contained three slits, as also illustrated in Figure \ref{fig:observations_akari_irc_fovs}. From smallest to largest, these slits are often referred to as the short slit, long slit and square slit. The square slit is also sometimes called the $1\arcmin \times 1\arcmin$ field of view, as it is essentially a miniature version of the $\sim 10\arcmin\times 10\arcmin$ FoV.

\begin{figure}
 \includegraphics[width=0.95\linewidth]{./graph/observations/akari_irc_fovs.png}
 \caption[The AKARI fields of view]{An illustration showing the sizes (in sexagecimal degrees) of the field of views observable with the three different IRC detectors. This figure has been reproduced from \citet{AKARI-IRC_manual} Figure 2.0.2.}
 \label{fig:observations_akari_irc_fovs}
\end{figure}

The imaging mode observations with each detector had a choice of 3 different filters, limiting the wavelength range to a narrower subset of the full detector range.
The multi-object spectroscopic capability was capable of dispersing the light of all stars in these fields of view with the help of a grism or (in the NIR detector) a prism dispersion element. The main practical difference between the grism and prism dispersion was that the grism dispersion gave much higher resolution ($R\approx 120$ at $\lambda=3.6\, {\rm \mu m}$) compared to the prism ($R\approx 12$ at $\lambda=3.5\, {\rm \mu m}$), but suffers from much more severe "confusion" (the overlap of dispersion from nearby objects) in the $10\arcmin \times 10\arcmin$ field of view. The properties of the various imaging and spectroscopic modes available to the IRC are summarised in Table \ref{tab:observations_AKARI_IRC}. The imaging filters and dispersion elements are mounted on independent filter wheel for each channel, and are selected during observations through rotation of the filter wheel.

\begin{table}
\begin{center}
\caption{A summary of the capabilities of the AKARI InfraRed Camera, in its various imaging and spectroscopic modes. Table adapted from \citet{AKARI-IRC_manual} Table 2.0.1.}
\begin{tabular}{|c|c|c|l|l|}
		\hline
		Detector	& \multicolumn{2}{c|}{Filter}& Wavelength			& Dispersion			\\
		channel		& name			& type		& range [${\rm \mu m}$]	& [${\rm \mu m / pix}$]	\\
		\hline
					& N2			& imaging	& $1.9-2.8$				& N/A					\\
					& N3$^{\rm (a)}$& imaging	& $2.7-3.8$				& N/A					\\
		NIR			& N4			& imaging	& $3.6-5.3$				& N/A					\\
					& NP$^{\rm (a)}$& prism		& $1.8-5.2$				& $0.06 @ 3.5\, {\rm \mu m}$ \\
					& NG$^{\rm (b)}$& grism		& $2.5-5.0$				& $0.0097$				\\
		\hline
					& S7			& imaging	& $5.9-8.4$				& N/A					\\
					& S9W			& imaging	& $6.7-11.6$			& N/A					\\
		MIR-S		& S11			& imaging	& $8.5-13.1$			& N/A					\\
					& SG1			& grism		& $5.4-8.4$				& $0.057$				\\
					& SG2			& grism		& $7.5-12.9$			& $0.099$				\\
		\hline
					& L15			& imaging	& $12.6-19.4$			& N/A					\\
					& L18W			& imaging	& $13.9-25.6$			& N/A					\\
		MIR-L		& L24			& imaging	& $20.3-26.5$			& N/A					\\
					& LG2			& grism		& $17.5-25.7$			& $0.175$				\\
		\hline
		\multicolumn{5}{l}{\footnotesize $\rm ^{(a)}$Data involving these filters will be discussed in Chapter \ref{chap:icemap}.}\\
		\multicolumn{5}{l}{\footnotesize $\rm ^{(b)}$Data involving this filter will be discussed in Chapter \ref{chap:cometh}.}\\
\end{tabular}
\label{tab:observations_AKARI_IRC}
\end{center}
\end{table}

NIR observations could be done simultaneously with MIR-S observations because of the way the IRC was constructed, and thus all observations taken in either channel automatically contain data from the other channel as well.

The AKARI data exploited in this thesis was gathered with the near-IR capabilities of the IRC during phase 2. Although MIR-S data for each observation was also available, it was not analysed for this thesis because to do so would have required the construction of a separate new pipeline specialized for the analysis of AKARI MIR with its own complications (such as having to contend with the strong zodiacal background light observed in the MIR).
\subsection{Observing with the IRC}
\begin{figure}
 \includegraphics[width=0.95\linewidth]{./graph/observations/akari_aot4.pdf}
 \caption[AKARI AOT4 progression]{The progression of the AKARI spectroscopic observing cycle AOT4 from left to right, top row to bottom row. Each exposure takes approximately 63 seconds, with the entire observing cycle (including overheads) lasting about 10 minutes. The telescope is liable to maneuver during the last two exposure cycles, making the scientific usefulness of the data contained in the last disperser exposure suspect. This figure has been adapted from \citet{AKARI-IRC_manual} Figure 2.2.6.}
 \label{fig:observations_akari_irc_aot4}
\end{figure}

Observations with the IRC were required to use exposure patterns ordered in a number of "astronomical observation templates" or AOTs. These AOTs cover a range of likely patterns to be useful in either imaging or spectroscopic observations, and operate as a mixture of exposure cycles, filter wheel rotations and micro-scan operations. Of these patterns, the one called AOT04 is the only spectroscopic exposure cycle, and was thus used for the observations exploited in this thesis. The progression of this exposure cycle is illustrated in Figure \ref{fig:observations_akari_irc_aot4}.

AOT04 produces a total of 8 frames of dispersed images and one frame taken with an imaging filter. Each AOT04 cycle also takes two dark exposures, both at the beginning and end of the cycle. Because of the way the observations are handled in conjunction with telescope attitude control, the end of AOT04 (and every other exposure cycle of AKARI) also had a chance of the telescope manoeuvring in the middle of exposure. When it happens, the last exposures become useless for scientific purposes, and should be ignored during data reduction. In AOT04 is the dispersed frames were taken either with the prism (NP) or grism (NG) dispersers, and the imaging frame taken in the middle of these observations was taken with the N3 filter. These observations are split into individual "pointings", each representing a single AOT observation and indicated by a 7-digit numerical code e.g. 4120021. Several pointings were repeated, and in such cases designated by an additional numeral preceded by a hyphen e.g. 4120021-001 and 4120021-002 for two repeated pointings. In this thesis I refer to such repeated pointings as "sub-pointings".

\subsection{Why AKARI?}
The observations analysed in Chapters \ref{chap:cometh} and \ref{chap:icemap} of this thesis were partially or completely acquired as part of the AKARI IMAPE (PI: Helen Fraser) ice mapping programme, which sought to determine the abundance of H$_2$O, CO and CO$_2$ ice towards several molecular cores. The target listing for the IMAPE programme is presented in Table \ref{tab:observations_imape}. These targets were selected on the basis of having been previously observed as part of the Spitzer legacy survey "cores to disks" \citep[C2D;][]{c2d} and would thus have much valuable supplementary data available to cross-correlate against the results of IMAPE.

\begin{table}
\begin{center}
\caption{The basic target information of the AKARI ice mapping programme IMAPE.}
\begin{tabular}{|l|ll|l|l|}
		\hline
		Pointing				& Core			& Disperser	& $\alpha_{\rm J2000}$	& $\delta_{\rm J2000}$	\\
		identifier				& name			& used		& [deg.]				& [deg.]				\\
		\hline
		4120002$\rm ^{(a)}$ 	& BHR59        & NG       	& 166.779 				&-62.097				\\
		4120004$\rm ^{(b)}$ 	& CB188        & NP       	& 290.067 				&+11.605				\\
		4120006$\rm ^{(b)}$ 	& DC274.2-00.4 & NP       	& 142.196 				&-51.612				\\
		4120007$\rm ^{(a)}$ 	& DC274.2-00.4 & NG       	& 142.196 				&-51.612				\\
		4120008$\rm ^{(b)}$ 	& DC275.9+01.9 & NP       	& 146.712 				&-51.107				\\
		4120009$\rm ^{(a)}$ 	& DC275.9+01.9 & NG       	& 146.712 				&-51.107				\\
		4120010$\rm ^{(b)}$ 	& DC291.0-03.5 & NP       	& 164.979 				&-63.738				\\
		4120011$\rm ^{(b)}$ 	& DC300.7-01.0 & NP       	& 187.871 				&-63.747				\\
		4120012$\rm ^{(b)}$ 	& DC346.0+07.8 & NP       	& 249.208 				&-35.618				\\
		4120018$\rm ^{(a)}$ 	& Mu8          & NG       	& 187.4024 				&-71.1775				\\
		4120021$\rm ^{(b)}$ 	& B35A         & NP       	& 086.1242 				&+09.1483				\\
		4120022$\rm ^{(a)}$ 	& B35A         & NG       	& 086.1242 				&+09.1483				\\
		4120023$\rm ^{(a)}$ 	& DC291.0-03.5 & NG       	& 164.979 				&-63.738				\\
		4120024$\rm ^{(a)}$ 	& DC300.7-01.0 & NG       	& 187.871 				&-63.747				\\
		4120034$\rm ^{(b)}$ 	& L1165        & NP       	& 331.7113 				&+59.0464				\\
		4120042$\rm ^{(b)}$ 	& DC269.4+03.0 & NP       	& 140.579 				&-45.815				\\
		4120043$\rm ^{(a)}$ 	& DC269.4+03.0 & NG       	& 140.579 				&-45.815				\\
		4121001$\rm ^{(b)}$ 	& BHR59        & NP       	& 166.779 				&-62.097				\\
		4121017$\rm ^{(b)}$ 	& Mu8          & NP       	& 187.4024 				&-71.1775				\\
		4121024$\rm ^{(a)}$ 	& DC300.7-01.0 & NG       	& 187.871 				&-63.747				\\
		4121035$\rm ^{(a)}$ 	& L1165        & NG       	& 331.7113 				&+59.0464				\\
		4121040$\rm ^{(c)}$ 	& BHR78        & NP       	& 189.079 				&-63.210				\\
		4121041$\rm ^{(a)}$ 	& BHR78        & NG       	& 189.079 				&-63.210				\\
		4121043$\rm ^{(a)}$ 	& DC269.4+03.0 & NG       	& 140.579 				&-45.815				\\
		4121044$\rm ^{(b)}$ 	& DC300.2-03.5 & NP       	& 186.054 				&-66.178				\\
		4121045$\rm ^{(a)}$ 	& DC300.2-03.5 & NG       	& 186.054 				&-66.178				\\
		\hline
		\multicolumn{5}{l}{\footnotesize $\rm ^{(a)}$Analysed in Chapter \ref{chap:cometh}.}\\
		\multicolumn{5}{l}{\footnotesize $\rm ^{(b)}$Analysed in Chapter \ref{chap:icemap}.}\\
		\multicolumn{5}{l}{\footnotesize $\rm ^{(c)}$Analysed in Chapter \ref{chap:icemap}, but no useful data could be extracted.}\\
\end{tabular}
\label{tab:observations_imape}
\end{center}
\end{table}

\begin{figure}
\begin{center}
 \includegraphics[width=0.65\linewidth]{./graph/observations/akari_confusion.pdf}
 \caption[How confusion happens in slitless spectroscopy]{An illustration of how spectral confusion happens with AKARI observations, and how confused sources may be partially deconvolved from each other in an ideal situation. The first observation (shown in the left picture) is made with such a roll angle of the telescope that the top of the frame is aligned with north. In this case the red end of the spectrum from source A is confused with the blue end of the spectrum for source B, while the spectrum of C is not confused at all. The right picture shows the second observation of the same field of view but with such a roll angle of the telescope that north points towards the right instead. In this case the spectrum of B is no longer confused, while the blue end of the spectrum of A is confused with the red end of the spectrum of C. If both observations had been made, the full spectrum of C and B could be recovered from the first and second observations, respectively, and most of the spectrum of A could be recovered by taking its blue end from the first and its red end from the second observation.}
 \label{fig:observations_akari_deconvolution}
\end{center}
\end{figure}

AKARI was first and foremost designed to operate as an imaging telescope and its spectroscopic capabilities were pushed to their limits with the IMAPE programme. To avoid confusion between observed spectra, the spectroscopic mode of AKARI was intended to be used primarily through either one of the three slits of the AKARI frame or with lone or well-separated sources observed in the "slitless" $10\arcmin \times 10\arcmin$ field of view. As will be shown in Chapter \ref{chap:icemap}, however, the molecular clouds observed as part of the IMAPE programme had tens if not hundreds of bright sources occupying the $10\arcmin \times 10\arcmin$ field of view, making spectral confusion inevitable. The observing programme of IMAPE had predicted this and tried to plan around it by requesting that repeated pointings of the same cloud be made in such a way that a large amount of time would pass between two observations of the same cloud. If the observations were made in such a way, AKARI would have a different roll angle (which could not otherwise be manually adjusted by the observers) when observing the same cloud, and deconvolving confused spectra would be made significantly easier. The principle behind this idea is illustrated in Figure \ref{fig:observations_akari_deconvolution}. The limitations of AKARI observing time allocation, however, caused these repeated pointings to be taken on subsequent orbits of the space telescope. This resulted in the telescope having practically the same roll angle on both observations and thus greatly hindered the effective deconvolution of most of the confused spectra.

Despite the difficulties first expected and then experienced in the data acquisition and reduction of its spectroscopic data, the AKARI space telescope is still the best instrument to have been available for the task of the type of ice mapping I am doing in this thesis.
The reason for using AKARI for ice mapping is that it was the only telescope capable of concurrent observations of multiple lines of sight in the 2 to 5 micron wavelength range. As was illustrated in Figure \ref{fig:intro_iceabs} of the previous chapter, Earth's atmosphere either severely hinders (as is the case for the 3-micron water ice feature)  or outright prevents (CO$_2$ at $\sim 4.3$ microns, although the IMAPE data relevant to this has already been analysed by \citealt{Noble2011} and \citealt{Noble2013}) the observation of several important ISM ice absorption features and thus makes ground-based observatories incapable of what the IMAPE programme sought to achieve. The only other telescopes which would have been capable of similar observations were the Infrared Space Observatory (ISO) and Spitzer, but ISO has been inoperational since 1998 and Spitzer is no longer capable of spectroscopy due to having run out coolant. Furthermore, no other observatory to date has been capable of simultaneous multi-object near-infrared spectroscopy with a field of view as large as $10$ by $10$ arc minutes. In the future the James Webb Telescope (JWST) is set to be capable of similar feats and more relative to what AKARI was capable of, but it is not scheduled to become operational until sometime after 2018.

\section{The Herschel space observatory}
\begin{figure}
 \includegraphics[width=0.98\linewidth]{./graph/observations/herschel_overview.png}
 \caption[The Herschel space telescope]{The Herschel space telescope. The left image shows the "warm" side of the telescope, and the right image shows the "cold" side. The middle image highlights the major components of the spacecraft. This figure has been reproduced from the Herschel observer's manual{\protect\footnotemark} Figure 2.1.}
 \label{fig:observations_herschel_overview}
\end{figure}
\footnotetext{\url{http://herschel.esac.esa.int/Docs/Herschel/pdf/observatory.pdf}}

The data analysed in Chapter \ref{chap:ysocomp} has been partially acquired from Herschel, and public pre-reduced SPIRE data has been made use of in the analysis in Chapter \ref{chap:icemap}.
\subsection{Mission summary}
The Herschel space observatory (formerly known as FIRST) was a mission operated by the European Space Agency (ESA) that performed imaging and photometry in the far-infrared and submillimeter parts of the spectrum. Its key science goals objectives included shedding light on the inter-relation between star and galaxy formation, the physics of the interstellar medium, astrochemistry, and planetary science studies. Herschel carried three science instruments: the two cameras/medium resolution spectrometers PACS and SPIRE, and a very high resolution heterodyne spectrometer HIFI. These instruments received their light from a 3.5-metre main mirror \citep{Herschel_paper}.

Herschel launched on 14$^{\rm th}$ May 2009, entering the Sun-Earth L2 point and beginning commissioning operations. The performance verification and science demonstration phases started soon after the commissioning operations, and the first routine observations were executed on 18$^{\rm th}$ October 2009. Herschel observations were generally split between priority 1 (OT1) and priority 2 (OT2) observations, with OT1 observations taking precedence over OT2. The operational lifetime of Herschel was limited by the supply of onboard liquid helium, which was used for cooling the telescope. After helium "boil off", the telescope would rapidly heat up to temperatures where observations in Herschel's sensitivity range were obscured by noise. This point was reached by the 29$^{\rm th}$ April 2013, and Herschel was sent to its final resting orbit around the Sun on the 17$^{\rm th}$ June 2013, marking the end of the mission. 

\subsection{Instrument overview}
Herschel carried three instruments: the Photodetector Array Camera and Spectrometer (PACS), the Spectral and Photometric Imaging REceiver (SPIRE),  and the Heterodyne Instrument for the Far Infrared (HIFI). Of these three, SPIRE and HIFI are relevant to this thesis.
\subsection{SPIRE}
The Spectral and Photometric Imaging REceiver (SPIRE) had both photometric and spectroscopic capabilities, but unlike PACS it operated at much longer wavelengths and could only (in spectrometer mode) produce sparsely sampled spectral maps within a $2\arcmin$ field of view.

The photometer had an option of three different bands to observe. They were centred at 250, 350, and 500 microns, and were called PSW, PMW and PLW, respectively. The spectrometer had two choices of wavelength ranges: SSW which scanned between 194 and 313 microns, and SLW which scanned between 303 and 671 microns. The spectrometer could produce spectra at three different spectral resolutions, compromising sensitivity for sharper spectra, although in practise the highest spectral resolution mode was almost always used. The different observing modes of SPIRE are summarized in Table \ref{tab:observations_herschel_spire}.

\begin{table}
\begin{center}
\caption{A summary of the capabilities of the SPIRE instrument of Herschel, in its various photometric and spectroscopic modes. Table adapted from the SPIRE handbook Table 2.1.}
\begin{tabular}{|l|c|c|c|c|c|}
%\multicolumn{2}{c|}{Filter}&
		\hline
		\hline
		\textbf{Sub-instrument}	&  \multicolumn{3}{c|}{\textbf{Photometer}}		& \multicolumn{2}{c|}{\textbf{Spectrometer}}		\\
		\hline
		\textbf{Array}			& \textbf{PSW}$\rm ^{(a)}$	& \textbf{PMW}	& \textbf{PLW}	& \textbf{SSW}	& \textbf{SLW}						\\
		\hline
		Band $[{\rm \mu m}]$	& 250			& 350			& 500			& 194-313		& 303-671							\\
		\hline
		Resolution $(\lambda / \Delta \lambda)$		& 3.3		& 3.4			& 2.5			& \multicolumn{2}{c|}{$\sim 40-1000\, @\, 250\,{\rm \mu m}$ (variable)}		\\
		\hline
		Unvignetted FoV			& \multicolumn{3}{c|}{$4\arcmin\times 8\arcmin$}& \multicolumn{2}{c|}{$2\arcmin$ (diameter)}				\\
		\hline
		Beam FWHM $[\arcsec]$	& 17.6			& 23.9			& 35.2			& 17-21			& 29-42								\\
		\hline
		\hline
		\multicolumn{6}{l}{\footnotesize $\rm ^{(a)}$Archive data acquired with this mode will be used in Chapter \ref{chap:icemap}.}\\
\end{tabular}
\label{tab:observations_herschel_spire}
\end{center}
\end{table}

Additional information about this instrument can be found from the SPIRE handbook\footnote{\url{http://herschel.esac.esa.int/Docs/SPIRE/spire_handbook.pdf}}.

\subsubsection{HIFI}
\begin{figure}
 \includegraphics[width=0.95\linewidth]{./graph/observations/herschel_hifi_spectrum.png}
 \caption[Early science demonstration Herschel/HIFI spectrum]{A famous spectrum acquired with HIFI during the early science demonstration phase of Herschel. The spectrum highlights several features of gas-phase water and organic molecules towards the Orion Nebula. \copyright ESA, HEXOS and the HIFI consortium. Image reproduced from the HIFI observer's manual{\protect\footnotemark}.}
 \label{fig:observations_herschel_hifi_spectrum}
\end{figure}
\footnotetext{\url{http://herschel.esac.esa.int/Docs/HIFI/pdf/hifi_om.pdf}}

The Heterodyne Instrument for the Far Infrared (HIFI) was --unlike PACS and SPIRE-- an instrument built for the sole purpose of producing high-resolution spectra. It could cover the frequency ranges of 480 to 1250 GHz ($625-240\, {\rm \mu m}$) and 1410 to 1910 GHz (($625-240\, {\rm \mu m}$) and was capable of observing in four different modes where could can trade spectral resolution for spectral coverage. At its highest resolution HIFI was capable of producing spectra at a resolution of 125 kHz, covering a frequency range of 230 MHz. The wide-band mode of HIFI covered a frequency range of 4 GHz, at a spectral resolution of 1 MHz. An example of a wide-band HIFI spectrum is presented in Figure \ref{fig:observations_herschel_hifi_spectrum}.

The observable frequency range was split into several bands, and one of these must be selected when preparing the observation. Each band had a different effective beam size, and the highest-frequency bands had a maximum allowable frequency range of 2.4 GHz. Information about the different bands are summarized in Table \ref{tab:observations_herschel_hifi}.

\begin{table}
\begin{center}
\caption{A summary of the frequency coverage and effective beam sizes and maximum bandwidths of the HIFI instrument of Herschel, in its available bands. Table adapted from the HIFI observer's manual{\protect\footnotemark} Table 3.1.}
\begin{tabular}{|l|l|l|l|}
%\multicolumn{2}{c|}{Filter}&
		\hline
		\textbf{Band}	& \textbf{Frequency coverage $[{\rm GHz}]$}	& \textbf{Beam size (HPBW)}	& \textbf{Max. bandwidth}	\\
		\hline
		1				& $488.1-628.4$								& $39\arcsec$				& $4\, {\rm GHz}$			\\
		\hline
		2				& $642.1-793.9$								& $30\arcsec$				& $4\, {\rm GHz}$			\\
		\hline
		3				& $807.1-952.9$								& $25\arcsec$				& $4\, {\rm GHz}$			\\
		\hline
		4$^{\rm (a)}$	& $957.2-1113.8$							& $21\arcsec$				& $4\, {\rm GHz}$			\\
		\hline
		5$^{\rm (a)}$	& $1116.2-1271.8$							& $19\arcsec$				& $4\, {\rm GHz}$			\\
		\hline
		6+7				& $1430.2-1901.8$							& $13\arcsec$				& $2.4\, {\rm GHz}$			\\
		\hline
		\multicolumn{4}{l}{\footnotesize $\rm ^{(a)}$The HIFI data exploited in Chapter \ref{chap:ysocomp} was acquired with these bands.}\\
\end{tabular}
\label{tab:observations_herschel_hifi}
\end{center}
\end{table}
\footnotetext{\url{http://herschel.esac.esa.int/Docs/HIFI/pdf/hifi_om.pdf}}

\subsection{Why Herschel?}
Much like AKARI was the only telescope available at the time (or in the near future) to conduct the kind of science goals laid out by IMAPE, Herschel was the only telescope capable of observing the gas-phase water sought in the Herschel key program WISH \citep[Water In Star-forming regions with Herschel; ][]{vanDishoeck2011}. Having observed a total of approximately 80 objects and having taken up 425 hours of Herschel's observing time, the many objectives of WISH revolved around understanding the formation and processing of gas-phase (and indirectly solid-state) water towards several star-forming regions. WISH achieved this by observing (among several other species; most notably CO and OH) H$_2$O emission and absorption in its various transitions ranging between $J=1$ and $J=9$. Many of these transitions have been completely unobservable by any other telescope before Herschel.

While not directly involved in the WISH project, the research presented in Chapter \ref{chap:ysocomp} makes extensive use of WISH data (for two H$_2$O and one CO gas-phase line) in its analysis. Thus it would not have been possible to make the discoveries of that chapter without the data available only from Herschel.

\section{Other observatories}
This thesis makes use of data acquired from a number of other observatories in addition to AKARI and Herschel. The other observatories do not provide data with a major role in this thesis. For this reason they do not have a lengthy description in their own sections, but all of them will be given a short introduction here, so they need not be reintroduced in later chapters and thus detract from the scientific narrative.
\subsection{The James Clerk Maxwell Telescope (JCMT)}
The James Clerk Maxwell Telescope (JCMT) is a ground-based single-dish telescope operating in the submillimetre wavelength regime. Having observed its first light in 1987, JCMT is located on top of Mauna Kea in Hawaii, and as of 2015 is operated by the East Asian Observatory.

The JCMT has been host to a number of instruments dedicated to both imaging and spectroscopy. Most notably regarding this thesis, some of the data analysed in Chapter \ref{chap:ysocomp} was acquired from the JCMT using its RxA and RxB receivers. Of these two, RxB (comprising a part of the RxW receiver) is no longer operational. Both RxA and RxB are dual sideband heterodyne receivers. The frequency window of RxA is $211-276\, {\rm GHz}$ and its beam has HPBW of $20\arcsec$. RxB covered the frequency window $325-375\, {\rm GHz}$ and its beam had HPBW of $14\arcsec$.

\subsection{The Very Large Telescope (VLT)}
The Very Large Telescope (VLT) is an optical interferometer consisting of four 8.2 meter main telescopes and a four 1.8 meter auxiliary telescopes. In addition to its interferometric capabilities, the  telescopes comprising VLT can be operated individually, and house a number of instruments suited for single-dish purposes. VLT is operated by the European Southern Observatory (ESO) and is situated at Cerro Paranal in Chile. The first of the VLT main telescopes received its first light in 1998.

Of the many instruments that are or have been used on the VLT, the now-de\-com\-mis\-si\-o\-ned ISAAC \citep[Infrared Spectrometer And Array Camera;][]{VLT_ISAAC} spectrometer is the one of relevance to this thesis. Some of the spectroscopic data analysed in Chapter \ref{chap:cometh} was acquired using the $2.55-5.1$ micron detection capabilities of ISAAC. ISAAC was also capable of observing in the wavelength range $0.98-2.5$ microns.
The instrument was mounted on the telescope UT3 (Melipal) and had both a low and medium resolution grating to be used in spectroscopy. Depending on selected slit size, the low resolution grating could produce spectra with a resolution ranging from $R=180$ to $R=1800$, and the medium resolution grating could produce spectra with $R=1000$ to $R=11500$.

\subsection{The Spitzer Space Telescope}
The Spitzer Space Telescope was an infrared space observatory which was operated by the National Aeronautics and Space Administration (NASA) and was launched in 2003. Before its supply of liquid helium was depleted in 2009, it was capable of both imaging and spectroscopy in the near to far infrared ($\sim 3-180\, {\rm \mu m}$) using its three onboard instruments IRAC (InraRed Array Camera), IRS (InfraRed Spectrograph), and MIPS (Multiband Imaging Photometer for Spitzer). In its current state (the Spitzer Warm Mission) only IRAC remains useful, and is capable of simultaneously observing at 3.6 and 4.5 microns. Chapter \ref{chap:icemap} of this thesis makes use of 8 micron maps obtained with IRAC when it was still capable of observing at that waveband. Furthermore, the C2D catalogue \citep{c2d} which was made use of in the same chapter was compiled using all four bands of IRAC (3.6, 4.5, 5.8 and 8.0 $\rm \mu m$).

\section{Concluding remarks}
What was presented above is merely the description of the telescopes. The data itself acquired from the telescopes will be discussed in their respective chapters. Some of this data --such as the previously published AKARI and VLT data discussed in Chapter \ref{chap:cometh}-- has already been reduced by others, and little extra work is needed to make it useful for the purposes of this thesis. Other data --like the JCMT CH$_3$OH data presented in Chapter \ref{chap:ysocomp}-- was non-reduced, but its reduction was a fairly straightforward process using well-established methods. The prism-dispersed slitless spectroscopy data from AKARI, however, is something which was both non-reduced and on which established methods could not be applied. For this reason a large part of Chapter \ref{chap:icemap} will be dedicated to describing the reduction process itself.

Obviously in exploiting archival data it is vital to undertake something new and different with the data being used. Prior to my research only the datasets from the AKARI $1\arcmin \times 1\arcmin$ NG ice spectra, and some of the data from the VLT/ISAAC observations had been discussed in previous publications \citep{Noble2013,Pontoppidan2003b}, although the same data had never been utilised for the purpose to which I exploit it in Chapter \ref{chap:cometh}. The AKARI NP data which form the basis of Chapter \ref{chap:icemap} and JCMT and Herschel data which form the basis of Chapter \ref{chap:ysocomp} had also not been previously published or analysed, with the exception of the Herschel H$_2$O and CO spectra in Chapter \ref{chap:ysocomp} which had previously been described in \citet{Kristensen2010} and \citet{Yldz2013}, respectively. Nevertheless, all the research in Chapters \ref{chap:cometh}-\ref{chap:icemap} of this thesis is unique.

The information presented in this chapter is gathered mostly from various manuals, press releases, and newsletters presented on the official websites of their respective telescopes. For further information on the telescopes, the reader is directed to their websites and especially the manuals contained therein.
%\printbibliography[segment=\therefsegment]