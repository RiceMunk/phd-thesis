Many of the molecules in interstellar space
are formed not in the gas phase, but in the solid state, in so-called
interstellar ices, and this thesis addresses our understanding of the
distribution and evolution of intestellar
ices, by analysing submillimeter and near-infrared observations of gas
and ice
towards a number of star forming and pre-stellar cores. The research is
focused around two key astronomical questions;
How is ice distributed in star forming regions?
How is ice affected by -- or affecting --  star formation processes?

There are three areas where this thesis contributes to this field:
(a) establishing the prevalence of methanol ice in star-forming regions, 
(b) understanding the formation and destruction
of water and methanol in the outflows of low-mass young stellar objects
(YSOs), and 
(c) generating the first the large-scale maps of water
ice towards several pre-stellar molecular clouds.

The three studies presented in this thesis exploit partially published 
archival data mainly from the AKARI
and Herschel space telescopes, and the ground-based ESO-VLT.
 To facilitate the reduction
and analysis of some of this data two major software
packages (\arf2 and \textsc{Omnifit}) were created 
with the \textsc{Python} programming language, and the
operation of both packages is fully documented in the
appendix of this thesis.

My research on methanol ice concludes that this molecule is
significantly more common in interstellar ices, particularly in
pre-stellar phases, than has been previously reported, with abundances
of up to 40 % with respect to that of water ice. I also confirm
that it is very likely to exist mixed as part of the so-called
"polar" ice component as opposed to "apolar" ice, a
result comensurate with our currently understanding of the
chemistry that generates methanol ice.
I investigated the destiny of this methanol ice during the
star-formation process in Chapter \ref{chap:ysocomp}.
I prove that high-temperature chemistry is a key route to water
formation in the warm post-shocked gas where YSO outflows impact
the colder gas and dust envelope.
In this same region methanol is sputtered from
grain surfaces, and my work shows that up to 99 % of is
destroyed in the process.
Finally in Chapter 5 I detail the novel observational technique
of ice mapping by slitless spectroscopy using the AKARI 
near-infrared camera. My research yields an unprecedented number of
water ice column density values towards hundreds of background star
lines of sight covering 12 separate dense molecular cores in as many
molecular clouds.
I illustrate through ice mapping how the spatial distribution of ice
varies in star-forming regions, and explore how ice column density in
pre-stellar regions is associated with dust opacity and extinction.

I complete this thesis by exploring how in the advent of JWST and 
E-ELT we might push further to understanding ice formation, evolution
and distribution processes and their role in star-formation.